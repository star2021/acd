FROM neo4j:latest

COPY ./Data/Covid_Weather_Data_Combined.csv /var/lib/neo4j/import

EXPOSE 7474 7473 7687
